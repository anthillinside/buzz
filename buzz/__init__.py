from __future__ import absolute_import

import logging
from config import Config

LOGGING_FORMAT = '%(asctime)s - %(levelname)s - Line %(lineno)d - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=Config.LOG_FORMAT)

logger = logging.getLogger(__name__)
