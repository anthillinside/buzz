from flask import g
import psycopg2
import psycopg2.pool
from config import Config
from buzz import logger


def get_buzz():
    try:
        cxn = _get_db_cxn()
    except psycopg2.Error:
        return None
    else:
        cur = cxn.cursor()
        cur.execute('select get_buzz() as buzz')
        if cur.rowcount == 1:
            return cur.fetchone()[0]
        return None


def add_buzz(table, new_template):
    try:
        cxn = _get_db_cxn()
    except psycopg2.Error:
        return False
    else:
        with cxn.cursor() as cur:
            cur.execute('insert into {} (val) values (%s)'.format(table), (new_template,))
            if cur.rowcount == 1:
                cxn.commit()
                return True
        return False


def _get_db_cxn():
    logger.debug('Entering database._get_db_cxn')
    pool = getattr(g, '_pool', None)
    if pool is None:
        logger.debug('_get_db_cxn: initializing connection pool')
        try:
            pool = g._pool = psycopg2.pool.SimpleConnectionPool(
                3,
                10,
                host=Config.PG_HOST,
                port=Config.PG_PORT,
                database=Config.PG_DB,
                user=Config.PG_USER,
                password=Config.PG_PASSWORD
            )
            cxn = pool.getconn()
            logger.debug('_get_db_cxn: cxn: {}'.format(cxn))
        except psycopg2.Error as err:
            logger.error('_get_db_cxn: Error obtaining PG connection: ({}): {}'.format(err.pgcode, err.pgerror))
            g._pool = None
            raise
    else:
        try:
            cxn = pool.getconn()
        except psycopg2.Error as err:
            logger.error('_get_db_cxn: Error obtaining PG connection: ({}): {}'.format(err.pgcode, err.pgerror))
            g._pool = None
            raise
    logger.debug('Leaving database._get_db_cxn')
    return cxn
