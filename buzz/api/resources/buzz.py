from flask_restful import reqparse, Resource
from ..misc import database as db


class Buzz(Resource):

    def get(self):
        return {"message": db.get_buzz()}, 200, {'X-Clacks-Overhead': 'GNU Terry Pratchett'}

    def post(self):
        post_parser = reqparse.RequestParser(trim=True, bundle_errors=True)
        post_parser.add_argument('adjective', type=str)
        post_parser.add_argument('adverb', type=str)
        post_parser.add_argument('noun', type=str)
        post_parser.add_argument('template', type=str)
        post_parser.add_argument('verb', type=str)

        args = post_parser.parse_args()

        add_ok = True
        for key in args:
            if not db.add_buzz(key, args[key]):
                add_ok = False
        if add_ok:
            return {}, 201, {'X-Sometimes-Dan-Smells-Like-Cheese': 'But not today'}
        else:
            return {}, 403, {'X-Sometimes-Dan-Smells-Like-Cheese': 'It\'s glandular!'}
