from flask_restful import Api
from buzz.api.resources.ekbuzz import Buzz
from config import Config

api = Api(prefix='/{}/v{}'.format(Config.PROJECT_NAME, Config.API_VERSION))

api.add_resource(Buzz, '/buzz/')
