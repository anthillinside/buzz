from flask import Flask

from buzz.api import api
from config import Config

app = Flask(__name__)

app.config.from_object(Config)

api.init_app(app)


if __name__ == '__main__':
    app.run()
