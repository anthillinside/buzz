from __future__ import print_function, absolute_import, division

import os
from config.baseconfig import BaseConfig, project_name, base_dir

_local_dir = os.path.join(base_dir, 'local_data')


class Config(BaseConfig):
    DEBUG = True
    TESTING = True

    # DATABASE
    PG_HOST = 'sbox_postgres_instance_here'
    PG_PORT = 5432
    PG_DB = 'buzz'
    PG_USER = 'sbox_postgres_user_here'
    PG_PASSWORD = 'sbox_postgres_pw_here'

    # LOGGING
    LOG_FILENAME = os.path.join(_local_dir, 'app.{}.log'.format(project_name))
