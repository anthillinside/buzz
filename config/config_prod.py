from __future__ import print_function, absolute_import, division

from config.baseconfig import BaseConfig


class Config(BaseConfig):
    PG_HOST = 'prod_postgres_instance_here'
    PG_PORT = 5432
    PG_DB = 'buzz'
    PG_USER = 'prod_postgres_user_here'
    PG_PASSWORD = 'prod_postgres_pw_here'
