from __future__ import print_function, absolute_import, division

import os
import logging
from datetime import timedelta
from socket import gethostname
import time


base_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))


class BaseConfig(object):
    PROJECT_NAME = 'buzz'
    DEBUG = False
    TESTING = False
    API_VERSION = 1

    # LOGGING
    LOG_FILENAME = "/tmp/{}/app.{}.log".format(PROJECT_NAME, PROJECT_NAME)
    LOGGER_NAME = "{}_log".format(PROJECT_NAME)
    LOG_LEVEL = logging.DEBUG
    LOG_DATE_FORMAT = '%Y-%m-%dT%H:%M:%S'
    LOG_FORMAT = '%(asctime)s - %(levelname)s - Line %(lineno)d - %(message)s'  # used by logging.Formatter
    LOG_TIME_CONVERTER = time.gmtime
    PERMANENT_SESSION_LIFETIME = timedelta(seconds=30)
