from __future__ import print_function, absolute_import, division

from config.baseconfig import BaseConfig


class Config(BaseConfig):
    PG_HOST = 'dev_postgres_instance_here'
    PG_PORT = 5432
    PG_DB = 'buzz'
    PG_USER = 'dev_postgres_user_here'
    PG_PASSWORD = 'dev_postgres_pw_here'
