from setuptools import setup, find_packages

config = {
    'name': 'buzz',
    'description': 'This service tells you things',
    'author': 'Kyle Olsen',
    'author_email': 'kyle.g.olsen@gmail.com',
    'url': 'https://bitbucket.org/anthillinside/buzz.git',
    'download_url': '',
    'maintainer': 'Kyle Olsen',
    'maintainer_email': 'kyle.g.olsen@gmail.com',
    'version': '0.1.0',
    'data_files': [],
    'package_data': {},
    'scripts': [],
    'install_requires': [
        'Flask==0.10.1',
        'Flask-Script==2.0.5',
        'Flask-RESTful==0.3.4',
    ],
    'packages': find_packages(),
    'zip_safe': False,
}

setup(**config)
