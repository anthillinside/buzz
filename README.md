Buzz
====================

Purpose
-------
Talks to you in buzzwordy gibberish.

Warning
--------
**This service is known to the state of California to cause cancer.**

Use
---
Endpoints:  
All endpoints begin /buzz/v1/

* GET /buzz/
* POST /buzz/

/buzz/ (GET)
---------------
This endpoint retrieves some text.

/buzz/ (POST)
----------------
This endpoint creates a new template for text.
