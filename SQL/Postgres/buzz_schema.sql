
CREATE TABLE IF NOT EXISTS adjective
(
    id SERIAL PRIMARY KEY,
    val TEXT NOT NULL
);
CREATE UNIQUE INDEX adj_uniq_cons ON adjective (val);

CREATE TABLE IF NOT EXISTS adverb
(
    id SERIAL PRIMARY KEY,
    val TEXT NOT NULL
);
CREATE UNIQUE INDEX adv_uniq_cons ON adverb (val);

CREATE TABLE IF NOT EXISTS noun
(
    id SERIAL PRIMARY KEY,
    val TEXT NOT NULL
);
CREATE UNIQUE INDEX nou_uniq_cons ON noun (val);

CREATE TABLE IF NOT EXISTS verb
(
    id SERIAL PRIMARY KEY,
    val TEXT NOT NULL
);
CREATE UNIQUE INDEX ver_uniq_cons ON verb (val);

CREATE TABLE IF NOT EXISTS subject
(
    id SERIAL PRIMARY KEY,
    val TEXT NOT NULL
);
CREATE UNIQUE INDEX sub_uniq_cons ON subject (val);

CREATE TABLE IF NOT EXISTS possessive
(
    id SERIAL PRIMARY KEY,
    val TEXT NOT NULL
);
CREATE UNIQUE INDEX pos_uniq_cons ON possessive (val);

CREATE TABLE IF NOT EXISTS template
(
    id SERIAL PRIMARY KEY,
    val TEXT NOT NULL
);
CREATE UNIQUE INDEX tem_uniq_cons ON template (val);

-- Pulls a template, does variable substitution, returns the Buzz
DROP FUNCTION IF EXISTS get_buzz();
CREATE OR REPLACE FUNCTION get_buzz()
  RETURNS TEXT AS $$
    DECLARE
      tpl TEXT;
      curr_tpl RECORD;
      curr_word TEXT;
    BEGIN
      SELECT val FROM template ORDER BY random() LIMIT 1 INTO tpl;
      FOR curr_tpl IN SELECT regexp_matches(tpl, '({(\w+)})', 'g') AS matches LOOP
        EXECUTE 'SELECT val FROM '|| quote_ident(curr_tpl.matches[2]) ||' ORDER BY random() LIMIT 1' INTO curr_word;
        tpl := regexp_replace(tpl, curr_tpl.matches[1], curr_word);
      END LOOP;
      RETURN tpl;
    END;
  $$ LANGUAGE plpgsql;
